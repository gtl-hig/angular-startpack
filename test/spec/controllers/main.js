'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('demoApp'));

  var MainCtrl, httpBackend, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $injector) {
    scope = $rootScope.$new();
    httpBackend = $injector.get('$httpBackend');
    httpBackend.whenGET('getData').respond({'answer': 'my data'});
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings).toBeDefined();
  });

  it('awesomeThings should be 3 elements long', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });

  it("has getData() method that makes HTTP GET request", function () {
    scope.getData();
    httpBackend.expectGET('getData');
    httpBackend.flush();    
    scope.$apply();
    expect(scope.data.answer).toEqual('my data');
  });

});
